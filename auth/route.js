const express = require('express');
const router = express.Router();
const { register, login, forget_password } = require('./auth');

router.route('/register').post(register);
router.route('/login').post(login);
router.route('/forget_password').post(forget_password);

module.exports = router;