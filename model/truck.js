const Mongoose = require('mongoose');

const TruckSchema = new Mongoose.Schema({
  created_by: {
    type: String,
    required: true
  },
  assigned_to: {
    type: String,
    default: 'Not assigned yet'
  },
  type: {
    type: String,
    enum: ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"],
    required: true
  },
  status: {
    type: String,
    enum: ["OL", "IS"],
    default: "IS"
  },
  createdDate: {
    type: String,
    required: true
  }
});

const Truck = Mongoose.model('truck', TruckSchema);
module.exports = Truck;