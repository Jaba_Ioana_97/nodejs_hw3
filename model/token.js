const Mongoose = require('mongoose');

const TokenSchema = new Mongoose.Schema({
  token: {
    type: String,
    require: true
  },
  expireAt: {
    type: Date,
    default: new Date(),
    expires: '180m',
  }
});

const Token = Mongoose.model('token', TokenSchema);
module.exports = Token;

// Resources
// https://stackoverflow.com/questions/71273672/mongoose-schema-set-an-expires-on-field-to-delete-document