const Mongoose = require('mongoose');

const UserSchema = new Mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    enum: ['driver', 'shipper'],
    required: true
  },
  hasCar: {
    type: Boolean,
    default: false
  },
  hasLoad: {
    type: Boolean,
    default: false
  },
  completedLoads: {
    type: String
  }
});

const User = Mongoose.model('user', UserSchema);
module.exports = User;