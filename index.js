require('dotenv').config();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('./model/user');
const Truck = require('./model/truck');
const Load = require('./model/load');
const Token = require('./model/token');

const express = require('express');
const { JsonWebTokenError } = require('jsonwebtoken');
const app = express();
const PORT = 8080;

const mongoose = require('mongoose');
const mongoString = process.env.DATABASE_URL;

mongoose.connect(mongoString);
const database = mongoose.connection;

database.on('error', (error) => {
    console.log('Error');
})

database.once('connected', () => {
    console.log('Database connected');
})

// middleware
app.use(express.json());

app.use((req, res, next) => {
    console.log('New request', `[${req.method}] ${req.url}`);
    next();
});

// authentication
app.use('/api/auth', require('./auth/route'));

// authorization
const { verifyToken } = require('./auth/auth');

// get profile info
app.get('/api/user/me', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            res.status(200).json({
                id: authData.user._id,
                role: authData.user.role,
                email: authData.user.email,
                createDate: new Date()
            })
        }
    })
})

// change user's password 
app.patch('/api/user/me/password', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            const { oldPassword, newPassword } = req.body;
            const id = authData.user._id;
            const result = await bcrypt.compare(oldPassword, authData.user.password);
            if(result) {
                const saltRounds = 10;
                const salt = bcrypt.genSaltSync(saltRounds);
                const hash = bcrypt.hashSync(newPassword, salt);

                await User.findByIdAndUpdate(id, { "password": hash })
                .then(() => 
                    res.status(200).json({ 
                        message: 'Password updated'
                    })
                )
                .catch(err =>
                    res.status(400).json({ 
                        message: 'An error occurred',
                        error: err.message
                    })
                )

                await Token.create({
                    token: req.token
                });
            } else {
                res.status(400).json({ 
                    message: `Password didn't match`
                })
            }
        }
    })
})

// delete account 
app.delete('/api/user/me', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            const id = authData.user._id;

            await User.findById(id)
            .then(user => user.remove())
            .then(() =>
                res.status(200).json({ 
                    message: 'User successfully deleted'
                })
            )
            .catch(err =>
                res.status(400).json({ 
                    message: 'An error occurred',
                    error: err.message
                })
            )

            await Token.create({
                token: req.token
            });
        }
    })
})

// add truck - role: driver
app.post('/api/trucks', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'driver') {
                res.status(400).json({
                    message: 'You must have driver role'
                })
            } else {
                const userId = authData.user._id;
                const { type } = req.body;
                const types = ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"];

                if(!types.includes(type.toUpperCase())) {
                    res.status(400).json(
                        { message: `Please choose a type between ${types}`}
                    )
                } else {
                    await Truck.create({
                        created_by: userId,
                        type: type.toUpperCase(),
                        createdDate: new Date()
                    })
                        .then(() => {
                            res.status(200).json({ 
                                message: 'Truck created sccessfully'
                            });
                        })
                        .catch((err) => {
                            res.status(400).json({ 
                                message: 'An error occurred',
                                error: err.message
                            });
                        })
                }
            }
        }
    })
})

// get user's trucks - role: driver
app.get('/api/trucks', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'driver') {
                res.status(400).json({
                    message: 'You must have driver role'
                })
            } else {
                const id = authData.user._id;

                await Truck.find({ created_by: id })
                    .then(truck =>
                        res.status(200).json({ 
                            message: truck
                        })
                    )
                    .catch(err =>
                        res.status(400).json({ 
                            message: 'An error occurred',
                            error: err.message
                        })
                    )
            }
        }
    })
})

// get user's truck by id - role: driver
app.get('/api/trucks/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'driver') {
                res.status(400).json({
                    message: 'You must have driver role'
                })
            } else {
                const id = req.params.id;

                await Truck.findById(id)
                .then(truck =>
                    res.status(200).json({ 
                        truck
                    })
                )
                .catch(err =>
                    res.status(400).json({ 
                        message: 'An error occurred',
                        error: err.message
                    })
                )
            }
        }
    })
})

// update user's truck by id - role: driver
app.put('/api/trucks/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'driver') {
                res.status(400).json({
                    message: 'You must have driver role'
                })
            } else {
                const id = req.params.id;
                const newType = req.body.type;
                const types = ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"];

                if(types.includes(newType.toUpperCase()) === false) {
                    res.status(400).json(
                        { message: `Please choose a type between ${types}`}
                    )
                } else {
                    await Truck.findByIdAndUpdate(id, {"type": newType.toUpperCase()})
                    .then(() =>
                        res.status(200).json({ 
                            message: 'Truck details changed successfully'
                        })
                    )
                    .catch(err =>
                        res.status(400).json({ 
                            message: 'An error occurred',
                            error: err.message
                        })
                    )
                }
            }
        }
    })
})

// delete user's truck by id - role: driver
app.delete('/api/trucks/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'driver') {
                res.status(400).json({
                    message: 'You must have driver role'
                })
            } else {
                const id = req.params.id;

                await Truck.findById(id)
                    .then(truck => {
                        truck.remove();
                    })
                    .then(() =>
                        res.status(200).json({ 
                            message: `Truck with id: ${id} , deleted successfully`
                        })
                    )
                    .catch(err =>
                        res.status(400).json({ 
                            message: 'An error occurred',
                            error: err.message
                        })
                    )
            }
        }
    })
})

// assign truck to user by id - role: driver
app.patch('/api/trucks/:id/assign', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'driver') {
                res.status(400).json({
                    message: 'You must have driver role'
                })
            } else {
                const id = req.params.id;
                const truck = await Truck.findById(id);
                // console.log(truck)

                if(truck.assigned_to === 'Not assigned yet') {
                    await Truck.findByIdAndUpdate(id, { $set:{ 'assigned_to' : authData.user._id } } )
                    .then(() => 
                        res.status(200).json({
                            message: 'Truck assigned successfully'
                        })
                    )
                    .catch((err) => 
                        res.status(400).json({
                            message: 'An error occurred',
                            error: err.message
                        })
                    )

                    await User.findByIdAndUpdate(authData.user._id, {'hasCar': true})
                } else {
                    res.status(400).json({
                        message: 'Truck is assigned to a driver'
                    })
                }
                
            }
        }
    })
})

// add Load for User - role: shipper
app.post('/api/loads', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'shipper') {
                res.status(400).json({
                    message: 'You must have shipper role'
                })
            } else {
                const userId = authData.user._id;
                const { name, payload, pickup_address, delivery_address, dimensions } = req.body;

                await Load.create({
                    name,
                    payload,
                    pickup_address,
                    delivery_address,
                    dimensions,
                    created_by: userId
                })
                    .then(() =>
                        res.status(200).json({
                            message: 'Load created successfully'
                        })
                    )
                    .catch((err) => 
                        res.status(400).json({
                            message: 'An error occurred',
                            error: err.message
                        })
                    )
            }
        }
    })
})

// get user's Load by id
app.get('/api/loads/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'shipper') {
                res.status(400).json({
                    message: 'You must have shipper role'
                })
            } else {
                const id = req.params.id;

                await Load.findById(id)
                    .then(load =>
                        res.status(200).json({ 
                            load
                        })
                    )
                    .catch(err =>
                        res.status(400).json({ 
                            message: 'An error occurred',
                            error: err.message
                        })
                    )
            }
        }
    })
})

// update user's load by id
app.put('/api/loads/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'shipper') {
                res.status(400).json({
                    message: 'You must have shipper role'
                })
            } else {
                const id = req.params.id;
                const { name, payload, pickup_address, delivery_address, dimensions } = req.body;

                await Load.findByIdAndUpdate(id, {
                    name,
                    payload,
                    pickup_address,
                    delivery_address,
                    dimensions
                })
                    .then(() =>
                        res.status(200).json({ 
                            message: 'Load details changed successfully'
                        })
                    )
                    .catch(err =>
                        res.status(400).json({ 
                            message: 'An error occurred',
                            error: err.message
                        })
                    )
            }
        }
    })
})

// delete user's load by id
app.delete('/api/loads/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'shipper') {
                res.status(400).json({
                    message: 'You must have shipper role'
                })
            } else {
                const id = req.params.id;

                await Load.findById(id)
                    .then(load => {
                        load.remove();
                    })
                    .then(() =>
                        res.status(200).json({ 
                            message: `Load with id: ${id} , deleted successfully`
                        })
                    )
                    .catch(err =>
                        res.status(400).json({ 
                            message: 'An error occurred',
                            error: err.message
                        })
                    )
            }
        }
    })
})

// post a user's load by id, search for drivers - role: shipper
// NEW --> POSTED
// if driver has a car and no load --> ASSIGNED
app.post('/api/loads/:id/post', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'shipper') {
                res.status(400).json({
                    message: 'You must have shipper role'
                })
            } else {
                const id = req.params.id;
                const load = await Load.findById(id);

                if(load.created_by === authData.user._id) {
                    await Load.findByIdAndUpdate(id, {"status": "POSTED"});

                    await User.find({"role": "driver"})
                        .then(async(user) => {
                            const drivers = [];

                            user.forEach(user => {
                                user.hasCar? user.hasLoad? console.log('user has already a load'): drivers.push(user): console.log(`user doesn't have assigned a car`)
                            });
                            // console.log(drivers)

                            if(drivers.length === 0) {
                                res.status(200).json({
                                    message: 'Load posted ssuccessfully',
                                    driver_found: false
                                })
                            } else {
                                const randomDriver = Math.floor(Math.random() * drivers.length); // random number = index
                                const driver = drivers[randomDriver];

                                await User.findByIdAndUpdate(driver._id, { "hasLoad": true });
                                await Load.findByIdAndUpdate(id, { "assigned_to": driver._id });
                                await Load.findByIdAndUpdate(id, { "status": "ASSIGNED" });
                                await Load.findByIdAndUpdate(id, { "logs":{ 
                                    "message": `Load assigned to driver with id ${driver._id}`,
                                    "time": new Date()
                                } });

                                res.status(200).json({
                                    message: 'Load posted ssuccessfully',
                                    driver_found: true
                                })
                            }
                        })
                        .catch((err) =>
                            res.status(400).json({
                                message: 'An error ocured',
                                error: err.message
                            })
                        )
                } else {
                    res.status(400).json({
                        message: `That load doesn't belong to that shipper`
                    })
                }
            }
        }
    })
})

// iterate to next Load state - role: driver
app.patch('/api/loads/active/state', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        const token = await Token.findOne({ token: req.token });
        if(token) {
            res.status(400).json({ 
                message: 'Error', 
                error: 'Invalid token' 
            })
        } else {
            if(authData.user.role !== 'driver') {
                res.status(400).json({
                    message: 'You must have driver role'
                })
            } else {
                await Load.findOne({"assigned_to": authData.user._id, "status": "ASSIGNED"})
                    .then(async (load) => {  
                        const loadStates = [
                            'En route to Pick Up', 
                            'Arrived to Pick Up', 
                            'En route to delivery', 
                            'Arrived to delivery'
                        ];

                        if(typeof(load.state) === 'undefined') {
                            let nextStateIndex = 0;
                            let loadState = loadStates[nextStateIndex];

                            await Load.findByIdAndUpdate(load._id, { $set:{ 'state' : loadState } });

                            res.status(200).json({
                                    message: `Load state changed to ${loadState}`
                            });
                        } else {
                            let nextStateIndex = loadStates.indexOf(load.state) + 1;
                            let loadState = loadStates[nextStateIndex];

                            if(loadState === 'Arrived to delivery') {
                                await Load.findByIdAndUpdate(load._id, { $set:{ 'state' : loadState } });
                                await Load.findByIdAndUpdate(load._id, { $set:{ 'status' : 'SHIPPED' } });
            
                                await User.findByIdAndUpdate(authData.user._id, { $set:{ 'hasLoad' : false } });
                                    

                                res.status(200).json({
                                    message: `Load state changed to ${loadState}`
                                });
                            } else {
                                await Load.findByIdAndUpdate(load._id, { $set:{ 'state' : loadState } });

                                res.status(200).json({
                                    message: `Load state changed to ${loadState}`
                                });
                            }
                        }
                    })
                    .catch(() => {
                        res.status(400).json({
                            message: `This driver has no load`
                        })
                    })
            }
        }
    })
})

// start the server 
app.listen(PORT, (err) => {
    if (err) {
      console.log('Error during server starting');
      return;
    }
    console.log(`Server Started at ${PORT}`);
});

// Resources
// https://errorsandanswers.com/best-practices-to-invalidate-jwt-while-changing-passwords-and-logout-in-node-js-closed/